# Table of Contents

* Introduction
* Technology / third party libraries
* Exercise requirements
* Sample commands
* Changing between storage type
* Others
* Source of inspiration
* Licence

## Introduction
Project developed as a personal showcase of abilities regarding:
- Java
- Spring Boot
- RESTFul API
- Hexagonal Architecture
- Best practices regarding TDD, SOLID.

Project doesn't provide any real bussiness value.

## Technology / third party libraries

- Java 11 (for development used: AdoptOpenJDK jdk-11.0.10.9-hotspot)
- Spring Boot 2.4.3
- Lombok 1.18.18
- JUnit 5 with extensions
- Open CSV - for CSV file processing
- H2 database
- SoapUI 5.5 - just for some load test fun

## Exercise requirements
Java developer exercise
- Prepare a simple service, which will let the client store and read payments. 

Functional requirements
- Payment is described by: unique identifier created during persistence process, amount, currency, user ID and target bank account number
- Payments should be stored by the service
- Service should expose an API, which should be able to:
   - Fetch payment resources
   - Create, update and delete payment resources
   - List a collection of payment resources
   
Non-Functional requirements
- API should be RESTFUL
- Application should be able to  store payments in a CSV file
- The code should be open for extensions, i.e  possibility to add a support for an in-memory database storage, so  the type of storage engine could be passed as a configuration parameter
- You should use best practices, for example TDD/BDD, SOLID etc.
- Consider using Clean Architecture or Hexagonal/Ports and Adapters patterns
- Try to simplify your code by using well  proven open source frameworks and libraries
- Write the code with production ready quality in mind 

Submitting the exercise
- Application code should be published on github or gitlab public repository.

## Sample commands
Create payment:

curl -X POST "http://localhost:8080/payments"  -H "Content-Type: application/json"  -d "{\"amount\":\"141.19\", \"ccy\":\"PLN\", \"userId\":\"jrybak\", \"accountNo\":\"PL83109024029774598514582961\"}"

Update payment:

curl -X PUT "http://localhost:8080/payments"  -H "Content-Type: application/json"  -d "{\"id\":\"1\",\"amount\":\"2467.19\", \"ccy\":\"GBP\", \"userId\":\"jrybak2\", \"accountNo\":\"PL83167024029774598514582961\"}"

Find payment by id:

curl -X GET "http://localhost:8080/payments/1"

Find all payments:

curl -X GET "http://localhost:8080/payments"

Delete payment by id

curl -X DELETE "http://localhost:8080/payments/1"

## Changing between storage type
Changing between storage type is possible via application.properties parameter: payment.hub.repository.provider
- CSV: Initial setting is for CVS file. File is stored in payment.hub.csv.datafile (application.properties).
Currently it is set to csv/payments.csv in resources, so it can be found in this directory:
/target/classes/csv/payments.csv 
- DUMMY: simple static HashTable
- JPA: for JPA repo H2 db was used. Data Source is initialized only for this repo.

## Others
In ../src/main/resources/soapui directory there is simple SoapUI REST-Payments-soapui-project.xml project.
It has simple load test define for POST, PUT and GET commands. 

## Source of inspiration
Source of inspiration, mainly:
- https://www.baeldung.com
- https://medium.com
- https://stackoverflow.com

## Licence :)
The MIT License (MIT)
