package com.jawo.paymenthub.domain;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentUnitTests {
    private static Validator validator;

    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void newPayment_ShouldCreate() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertTrue(violations.isEmpty());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidIdNotPositive() {
        //Given
        final Payment payment = new Payment(-1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAmountNull() {
        //Given
        final Payment payment = new Payment(1L, null, "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAmountNotPositive() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(-1), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyNull() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), null, "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyEmpty() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyBlank() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "   ", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyToShort() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PL", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyToLong() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLNO", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidUserIdNull() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", null, "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidUserIdEmpty() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidUserIdBlank() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "  ", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidUserIdToLong() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", RandomStringUtils.randomAlphabetic(65), "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAccountNoNull() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", null);

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAccountNoEmpty() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAccountNoBlank() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "  ");

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAccountNoToLong() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", RandomStringUtils.randomAlphabetic(35));

        //When
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        //Then
        assertEquals(1, violations.size());
    }
}
