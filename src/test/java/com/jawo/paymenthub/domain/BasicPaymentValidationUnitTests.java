package com.jawo.paymenthub.domain;

import com.jawo.paymenthub.domain.validators.BasicPaymentValidation;
import com.jawo.paymenthub.domain.validators.PaymentValidation;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

public class BasicPaymentValidationUnitTests {

    @Test
    void validateCcy_shouldValidateCcy_Valid() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        final BasicPaymentValidation basicPaymentValidation = new BasicPaymentValidation(payment);

        basicPaymentValidation.validateCcy();

        //Then
        assertFalse(basicPaymentValidation.isInvalid());
    }

    @Test
    void validateCcy_shouldNotValidateCcy_Invalid() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "aaa", "ikrasicki", "PL33109024024617497416165687");
        final BasicPaymentValidation basicPaymentValidation = new BasicPaymentValidation(payment);

        basicPaymentValidation.validateCcy();

        //Then
        assertTrue(basicPaymentValidation.isInvalid());
        assertTrue(basicPaymentValidation.getMessages().containsKey(PaymentValidation.CCY));
        assertEquals(basicPaymentValidation.getMessages().get(PaymentValidation.CCY), BasicPaymentValidation.INVALID_CURRENCY);
    }

    @Test
    void validateCcy_shouldValidateUserId_Valid() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        final BasicPaymentValidation basicPaymentValidation = new BasicPaymentValidation(payment);

        basicPaymentValidation.validateUserId();

        //Then
        assertFalse(basicPaymentValidation.isInvalid());
    }

    @Test
    void validateCcy_shouldNotValidateUserId_InvalidNotAlphanumeric() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikr,asicki", "PL33109024024617497416165687");
        final BasicPaymentValidation basicPaymentValidation = new BasicPaymentValidation(payment);

        basicPaymentValidation.validateUserId();

        //Then
        assertTrue(basicPaymentValidation.isInvalid());
        assertTrue(basicPaymentValidation.getMessages().containsKey(PaymentValidation.USER_ID));
        assertEquals(basicPaymentValidation.getMessages().get(PaymentValidation.USER_ID), BasicPaymentValidation.INVALID_USER_ID);
    }

    @Test
    void validateCcy_shouldNotValidateUserId_InvalidEol() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki\n", "PL33109024024617497416165687");
        final BasicPaymentValidation basicPaymentValidation = new BasicPaymentValidation(payment);

        basicPaymentValidation.validateUserId();

        //Then
        assertTrue(basicPaymentValidation.isInvalid());
        assertTrue(basicPaymentValidation.getMessages().containsKey(PaymentValidation.USER_ID));
        assertEquals(basicPaymentValidation.getMessages().get(PaymentValidation.USER_ID), BasicPaymentValidation.INVALID_USER_ID);
    }

    @Test
    void validateCcy_shouldValidateAccountNo_Valid() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        final BasicPaymentValidation basicPaymentValidation = new BasicPaymentValidation(payment);

        basicPaymentValidation.validateAccountNo();

        //Then
        assertFalse(basicPaymentValidation.isInvalid());
    }

    @Test
    void validateCcy_shouldNotValidateAccountNo_InvalidIban() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "33109024024617497416165687");
        final BasicPaymentValidation basicPaymentValidation = new BasicPaymentValidation(payment);

        //When
        basicPaymentValidation.validateAccountNo();

        //Then
        assertTrue(basicPaymentValidation.isInvalid());
        assertTrue(basicPaymentValidation.getMessages().containsKey(PaymentValidation.ACCOUNT_NO));
        assertEquals(basicPaymentValidation.getMessages().get(PaymentValidation.ACCOUNT_NO), BasicPaymentValidation.INVALID_ACCOUNT);
    }

    @Test
    void validateCcy_shouldNotValidateAccountNo_InvalidEol() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL331090240246174974161656\n");
        final BasicPaymentValidation basicPaymentValidation = new BasicPaymentValidation(payment);

        //When
        basicPaymentValidation.validateAccountNo();

        //Then
        assertTrue(basicPaymentValidation.isInvalid());
        assertTrue(basicPaymentValidation.getMessages().containsKey(PaymentValidation.ACCOUNT_NO));
        assertEquals(basicPaymentValidation.getMessages().get(PaymentValidation.ACCOUNT_NO), BasicPaymentValidation.INVALID_ACCOUNT);
    }
}
