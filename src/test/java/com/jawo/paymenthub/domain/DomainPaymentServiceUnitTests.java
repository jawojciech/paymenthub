package com.jawo.paymenthub.domain;

import com.jawo.paymenthub.domain.exceptions.IllegalArgumentExceptionWithMap;
import com.jawo.paymenthub.domain.validators.BasicPaymentValidation;
import com.jawo.paymenthub.domain.validators.PaymentValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.any;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class DomainPaymentServiceUnitTests {
    private PaymentRepository paymentRepository;
    private DomainPaymentService paymentService;

    @BeforeEach
    void setUp() {
        paymentRepository = mock(PaymentRepository.class);
        paymentService = new DomainPaymentService(paymentRepository);
    }

    @Test
    void findById() {
        //Given
        //When
        Optional<Payment> payment = paymentService.findPaymentById(1);

        //Then
        verify(paymentRepository, times(1)).findById(any(Long.class));
    }

    @Test
    void findAllPayments() {
        //Given
        //When
        List<Payment> payments = paymentService.findAllPayments();

        //Then
        verify(paymentRepository, times(1)).findAll();
    }

    @Test
    void createPayment_shouldCreatePayment() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        when(paymentRepository.create(any(Payment.class))).thenReturn(payment);

        //When
        paymentService.createPayment(payment);

        //Then
        verify(paymentRepository, times(1)).create(any(Payment.class));
    }

    @Test
    void createPayment_shouldNotCreatePayment_InvalidCcy() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "aLN", "ikrasicki", "PL33109024024617497416165687");

        when(paymentRepository.create(any(Payment.class))).thenReturn(payment);
        try {
            //When
            Payment result = paymentService.createPayment(payment);

            //Then
            fail();
        } catch(IllegalArgumentExceptionWithMap e) {
            assertTrue(e.getMessages().get(PaymentValidation.CCY).contains(BasicPaymentValidation.INVALID_CURRENCY));
        }
    }

    @Test
    void createPayment_shouldNotCreatePayment_InvalidAccountNo() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "33109024024617497416165687");

        when(paymentRepository.create(any(Payment.class))).thenReturn(payment);
        try {
            //When
            Payment result = paymentService.createPayment(payment);

            //Then
            fail();
        } catch(IllegalArgumentExceptionWithMap e) {
            assertTrue(e.getMessages().get(PaymentValidation.ACCOUNT_NO).contains(BasicPaymentValidation.INVALID_ACCOUNT));
        }
    }

    @Test
    void createPayment_shouldNotCreatePayment_InvalidCcyAndAccountNo() {
        //Given
        final Payment payment = new Payment(null, new BigDecimal(100), "abc", "ikrasicki", "33109024024617497416165687");
        when(paymentRepository.create(any(Payment.class))).thenReturn(payment);
        try {
            //When
            Payment result = paymentService.createPayment(payment);

            //Then
            fail();
        } catch(IllegalArgumentExceptionWithMap e) {
            assertTrue(e.getMessages().get(PaymentValidation.CCY).contains(BasicPaymentValidation.INVALID_CURRENCY));
            assertTrue(e.getMessages().get(PaymentValidation.ACCOUNT_NO).contains(BasicPaymentValidation.INVALID_ACCOUNT));
        }
    }

    @Test
    void updatePayment_shouldUpdatePayment() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        paymentService.updatePayment(payment);

        //Then
        verify(paymentRepository, times(1)).update(any(Payment.class));
    }

    @Test
    void updatePayment_shouldNotUpdatePayment_InvalidCcy() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "123", "ikrasicki", "PL33109024024617497416165687");
        try {
            //When
            paymentService.updatePayment(payment);

            //Then
            fail();
        } catch(IllegalArgumentExceptionWithMap e) {
            assertTrue(e.getMessages().get(PaymentValidation.CCY).contains(BasicPaymentValidation.INVALID_CURRENCY));
        }
    }

    @Test
    void updatePayment_shouldNotUpdatePayment_InvalidAccountNo() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "33109024024617497416165687");
        try {
            //When
            paymentService.updatePayment(payment);

            //Then
            fail();
        } catch(IllegalArgumentExceptionWithMap e) {
            assertTrue(e.getMessages().get(PaymentValidation.ACCOUNT_NO).contains(BasicPaymentValidation.INVALID_ACCOUNT));
        }
    }

    @Test
    void deletePayment_shouldDeletePayment() {
        //Given
        //When
        paymentService.deletePayment(1);

        //Then
        verify(paymentRepository, times(1)).deleteById(any(Long.class));
    }
}
