package com.jawo.paymenthub.infra.repo.helper;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.jawo.paymenthub.infra.repo.helper")
public class RepoTestConfiguration {

}
