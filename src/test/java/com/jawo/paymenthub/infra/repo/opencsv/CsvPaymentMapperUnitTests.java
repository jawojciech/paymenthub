package com.jawo.paymenthub.infra.repo.opencsv;

import com.jawo.paymenthub.domain.Payment;
import com.jawo.paymenthub.infra.repo.helper.RepoTestConfiguration;
import com.jawo.paymenthub.infra.repo.helper.RepoTestExtension;
import com.jawo.paymenthub.infra.repo.helper.RepoTestHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.shadow.com.univocity.parsers.csv.Csv;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CsvPaymentMapperUnitTests {
    CsvPaymentMapper paymentMappper;

    @BeforeEach
    void setUp() {
        paymentMappper = new CsvPaymentMapper();
    }

    @Test
    void map_shouldMapDomain2Csv() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100.54), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        String[] csvPayment = paymentMappper.map(payment);

        //Then
        assertEquals(payment.getId(), Long.parseLong(csvPayment[CsvFieldPosition.ID.ordinal()]));
        assertEquals(payment.getAmount(), new BigDecimal(csvPayment[CsvFieldPosition.AMOUNT.ordinal()]));
        assertEquals(payment.getCcy(), csvPayment[CsvFieldPosition.CCY.ordinal()]);
        assertEquals(payment.getUserId(), csvPayment[CsvFieldPosition.USER_ID.ordinal()]);
        assertEquals(payment.getAccountNo(), csvPayment[CsvFieldPosition.ACCOUNT_NO.ordinal()]);
    }

    @Test
    void map_shouldMapCsv2Domain() {
        //Given
        final String[] csvPayment = new String[] { "1", "100.45", "PLN", "ikrasicki", "PL33109024024617497416165687" };

        //When
        Payment payment = paymentMappper.map(csvPayment);

        //Then
        assertEquals(Long.parseLong(csvPayment[CsvFieldPosition.ID.ordinal()]), payment.getId());
        assertEquals(new BigDecimal(csvPayment[CsvFieldPosition.AMOUNT.ordinal()]), payment.getAmount());
        assertEquals(csvPayment[CsvFieldPosition.CCY.ordinal()], payment.getCcy());
        assertEquals(csvPayment[CsvFieldPosition.USER_ID.ordinal()], payment.getUserId());
        assertEquals(csvPayment[CsvFieldPosition.ACCOUNT_NO.ordinal()], payment.getAccountNo());
    }
}
