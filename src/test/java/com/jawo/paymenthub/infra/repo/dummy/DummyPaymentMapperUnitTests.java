package com.jawo.paymenthub.infra.repo.dummy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.jawo.paymenthub.domain.Payment;
import java.math.BigDecimal;

public class DummyPaymentMapperUnitTests {
    DummyPaymentMapper paymentMappper;

    @BeforeEach
    void setUp() {
        paymentMappper = new DummyPaymentMapper();
    }

    @Test
    void map_shouldMapDomain2Dummy() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        DummyPayment dummyPayment = paymentMappper.map(payment);

        //Then
        assertEquals(payment.getId(), dummyPayment.getId());
        assertEquals(payment.getAmount(), dummyPayment.getAmount());
        assertEquals(payment.getAccountNo(), dummyPayment.getAccountNo());
        assertEquals(payment.getCcy(), dummyPayment.getCcy());
        assertEquals(payment.getUserId(), dummyPayment.getUserId());
    }

    @Test
    void map_shouldMapDummy2Domain() {
        //Given
        final DummyPayment dummyPayment = new DummyPayment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Payment payment = paymentMappper.map(dummyPayment);

        //Then
        assertEquals(payment.getId(), dummyPayment.getId());
        assertEquals(payment.getAmount(), dummyPayment.getAmount());
        assertEquals(payment.getAccountNo(), dummyPayment.getAccountNo());
        assertEquals(payment.getCcy(), dummyPayment.getCcy());
        assertEquals(payment.getUserId(), dummyPayment.getUserId());
    }
}
