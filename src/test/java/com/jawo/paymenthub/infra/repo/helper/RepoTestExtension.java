package com.jawo.paymenthub.infra.repo.helper;

import org.junit.jupiter.api.extension.*;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

public class RepoTestExtension implements TestInstancePostProcessor, ParameterResolver{
    private ApplicationContext applicationContext;

    @Override
    public void postProcessTestInstance(Object testInstance, ExtensionContext extensionContext) {
        applicationContext = SpringExtension.getApplicationContext(extensionContext);
    }

    public RepoTestHelper getRepoTestHelper() {
        return applicationContext.getBean(RepoTestHelper.class);
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == RepoTestHelper.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return getRepoTestHelper();
    }
}
