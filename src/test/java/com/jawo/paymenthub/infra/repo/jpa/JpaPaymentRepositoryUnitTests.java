package com.jawo.paymenthub.infra.repo.jpa;

import com.jawo.paymenthub.domain.Payment;
import com.jawo.paymenthub.infra.repo.helper.RepoTestConfiguration;
import com.jawo.paymenthub.infra.repo.helper.RepoTestExtension;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith({ SpringExtension.class, RepoTestExtension.class})
@ContextConfiguration(classes = RepoTestConfiguration.class)
public class JpaPaymentRepositoryUnitTests {
    private JpaPaymentRepository jpaPaymentRepository;
    private JpaPaymentRepositoryAdapter paymentRepositoryAdapter;
    private AutoCloseable closeable;
    private JpaPaymentMapper paymentMapper;

    @BeforeEach
    public void setUpEach() {
        jpaPaymentRepository = mock(JpaPaymentRepository.class);
        paymentMapper = new JpaPaymentMapper();
        paymentRepositoryAdapter = new JpaPaymentRepositoryAdapter(paymentMapper, jpaPaymentRepository);
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }

    @Test
    public void findById_ShouldFind_Exists() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        JpaPayment jpaPayment = new JpaPayment(payment.getAmount(), payment.getCcy(), payment.getUserId(), payment.getAccountNo());
        jpaPayment.setId(payment.getId());
        Optional<JpaPayment> jpaPaymentOptional = Optional.of(jpaPayment);
        when(jpaPaymentRepository.findById(any(Long.class))).thenReturn(jpaPaymentOptional);

        //When
        Optional<Payment> result = paymentRepositoryAdapter.findById(1L);

        //Then
        assertTrue(result.isPresent());
    }

    @Test
    public void findById_ShouldNotFind_NotExists() {
        //Given
        final Optional<JpaPayment> jpaPayment = Optional.empty();
        when(jpaPaymentRepository.findById(any(Long.class))).thenReturn(jpaPayment);

        //When
        Optional<Payment> result = paymentRepositoryAdapter.findById(1L);

        //Then
        assertTrue(result.isEmpty());
    }

    @Test
    public void create_ShouldCreate() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        JpaPayment jpaPayment = new JpaPayment(payment.getAmount(), payment.getCcy(), payment.getUserId(), payment.getAccountNo());
        jpaPayment.setId(payment.getId());
        when(jpaPaymentRepository.save(any(JpaPayment.class))).thenReturn(jpaPayment);

        //When
        Payment result = paymentRepositoryAdapter.create(payment);

        //Then
        assertEquals(payment.getId(), result.getId());
        assertEquals(payment.getUserId(), result.getUserId());
        assertEquals(payment.getAmount(), result.getAmount());
        assertEquals(payment.getCcy(), result.getCcy());
        assertEquals(payment.getAccountNo(), result.getAccountNo());
    }

    @Test
    public void update_ShouldUpdate_Exists() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        JpaPayment jpaPayment = new JpaPayment(payment.getAmount(), payment.getCcy(), payment.getUserId(), payment.getAccountNo());
        jpaPayment.setId(payment.getId());
        Optional<JpaPayment> jpaPaymentOptional = Optional.of(jpaPayment);
        when(jpaPaymentRepository.findById(any(Long.class))).thenReturn(jpaPaymentOptional);
        when(jpaPaymentRepository.save(any(JpaPayment.class))).thenReturn(jpaPayment);

        //When
        Optional<Payment> result = paymentRepositoryAdapter.update(payment);

        //Then
        assertTrue(result.isPresent());
        assertEquals(payment.getId(), result.get().getId());
        assertEquals(payment.getUserId(), result.get().getUserId());
        assertEquals(payment.getAmount(), result.get().getAmount());
        assertEquals(payment.getCcy(), result.get().getCcy());
        assertEquals(payment.getAccountNo(), result.get().getAccountNo());
    }

    @Test
    public void update_ShouldNotUpdate_NotExists() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        Optional<JpaPayment> jpaPaymentOptional = Optional.empty();
        when(jpaPaymentRepository.findById(any(Long.class))).thenReturn(jpaPaymentOptional);

        //When
        Optional<Payment> result = paymentRepositoryAdapter.update(payment);

        //Then
        assertTrue(result.isEmpty());
    }

    @Test
    public void deleteById_ShouldRunDeleteById() {
        //Given
        //When
        paymentRepositoryAdapter.deleteById(1L);

        //Then
        verify(jpaPaymentRepository, times(1)).deleteById(any(Long.class));
    }
    @Test
    public void deleteAllPayments_ShouldRunDeleteAll() {
        //Given
        //When
        paymentRepositoryAdapter.deleteAllPayments();

        //Then
        verify(jpaPaymentRepository, times(1)).deleteAll();
    }
}
