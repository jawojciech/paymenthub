package com.jawo.paymenthub.infra.repo.helper;

import com.jawo.paymenthub.domain.Payment;
import com.jawo.paymenthub.domain.PaymentRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Service
public class RepoTestHelper {

    public void findById_ShouldFind_Exists(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        Payment newPayment = paymentRepository.create(payment);

        //When
        Optional<Payment> result = paymentRepository.findById(newPayment.getId());

        //Then
        assertTrue(result.isPresent());
    }

    public void findById_ShouldNotFind_NotExists(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        Payment newPayment = paymentRepository.create(payment);
        assertTrue(newPayment.getId() > 0);

        //When
        Optional<Payment> result = paymentRepository.findById(newPayment.getId() + 1);

        //Then
        assertEquals(true, result.isEmpty());
    }

    public void findAll_ShouldFind_Exists(PaymentRepository paymentRepository) {
        //Given
        long count = paymentRepository.findAll().size();
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        paymentRepository.create(payment);
        payment = new Payment(null, new BigDecimal(120), "EUR", "kkowalski", "PL33109024024617497416165634");
        paymentRepository.create(payment);

        //When
        int result = paymentRepository.findAll().size();

        //Then
        assertEquals(count + 2, result);
    }

    public void findAll_ShouldNotFind_NotExists(PaymentRepository paymentRepository) {
        //Given
        //When
        paymentRepository.deleteAllPayments();

        //Then
        assertEquals(0, paymentRepository.findAll().size());
    }

    public void create_ShouldCreate(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Payment newPayment = paymentRepository.create(payment);

        //Then
        assertTrue(paymentRepository.findById(newPayment.getId()).isPresent());
        assertEquals("PLN", paymentRepository.findById(newPayment.getId()).get().getCcy());
    }

    public void create_ShouldCreate_CommaInUserId(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasi,cki", "PL33109024024617497416165687");

        //When
        Payment newPayment = paymentRepository.create(payment);

        //Then
        assertTrue(paymentRepository.findById(newPayment.getId()).isPresent());
        assertEquals("PLN", paymentRepository.findById(newPayment.getId()).get().getCcy());
    }

    public void create_ShouldCreate_EndOfLineInUserId(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasi,cki\n", "PL33109024024617497416165687");

        //When
        Payment newPayment = paymentRepository.create(payment);

        //Then
        assertTrue(paymentRepository.findById(newPayment.getId()).isPresent());
        assertEquals("PLN", paymentRepository.findById(newPayment.getId()).get().getCcy());
    }

    public void update_ShouldUpdate_Exists(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        Payment newPayment = paymentRepository.create(payment);
        assertTrue(paymentRepository.findById(newPayment.getId()).isPresent());
        assertEquals( "PLN", paymentRepository.findById(newPayment.getId()).get().getCcy());
        payment.setId(newPayment.getId());
        payment.setCcy("USD");
        payment.setAmount(new BigDecimal(100.12));
        payment.setAccountNo("PL33109024024617497416165688");
        payment.setUserId("fszopen");

        //When
        Optional<Payment> updPayment = paymentRepository.update(payment);

        //Then
        assertEquals("USD", updPayment.get().getCcy());
        assertEquals(new BigDecimal(100.12), updPayment.get().getAmount());
        assertEquals("PL33109024024617497416165688", updPayment.get().getAccountNo());
        assertEquals("fszopen", updPayment.get().getUserId());
    }

    public void update_ShouldNotUpdate_NotExists(PaymentRepository paymentRepository) {
        //Given
        long id = 1L;
        paymentRepository.deleteById(id);
        assertTrue(paymentRepository.findById(id).isEmpty());
        Payment payment = new Payment(id, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Optional<Payment> updPayment = paymentRepository.update(payment);

        //Then
        assertTrue(updPayment.isEmpty());
        assertTrue(paymentRepository.findById(id).isEmpty());
    }

    public void delete_ShouldDelete_Exists(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        Payment newPayment = paymentRepository.create(payment);
        assertTrue(paymentRepository.findById(newPayment.getId()).isPresent());

        //When
        paymentRepository.deleteById(newPayment.getId());

        //Then
        assertTrue(paymentRepository.findById(newPayment.getId()).isEmpty());
    }

    public void delete_ShouldNotDelete_NotExists(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        Payment newPayment = paymentRepository.create(payment);
        long count = paymentRepository.findAll().size();

        //When
        paymentRepository.deleteById(newPayment.getId() + 1);

        //Then
        assertEquals(count, paymentRepository.findAll().size());
    }

    public void deleteAll_ShouldDeleteAll(PaymentRepository paymentRepository) {
        //Given
        Payment payment = new Payment(null, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");
        paymentRepository.create(payment);
        payment = new Payment(null, new BigDecimal(120), "EUR", "kkowalski", "PL33109024024617497416165634");
        paymentRepository.create(payment);

        //When
        paymentRepository.deleteAllPayments();

        //Then
        assertEquals(0, paymentRepository.findAll().size());
    }
}
