package com.jawo.paymenthub.infra.repo.jpa;

import com.jawo.paymenthub.domain.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JpaPaymentMapperUnitTests {
    JpaPaymentMapper paymentMappper;

    @BeforeEach
    void setUp() {
        paymentMappper = new JpaPaymentMapper();
    }
    
    @Test
    void map_shouldMapDomain2Jpa() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        final JpaPayment jpaPayment = paymentMappper.map(payment);

        //Then
        assertEquals(payment.getId(), jpaPayment.getId());
        assertEquals(payment.getAmount(), jpaPayment.getAmount());
        assertEquals(payment.getAccountNo(), jpaPayment.getAccountNo());
        assertEquals(payment.getCcy(), jpaPayment.getCcy());
        assertEquals(payment.getUserId(), jpaPayment.getUserId());
    }

    @Test
    void map_shouldMapJpa2Domain() {
        //Given
        final JpaPayment jpaPayment = new JpaPayment(new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        final Payment payment = paymentMappper.map(jpaPayment);

        //Then
        assertEquals(jpaPayment.getId(), payment.getId());
        assertEquals(jpaPayment.getAmount(), payment.getAmount());
        assertEquals(jpaPayment.getAccountNo(), payment.getAccountNo());
        assertEquals(jpaPayment.getCcy(), payment.getCcy());
        assertEquals(jpaPayment.getUserId(), payment.getUserId());
    }
}
