package com.jawo.paymenthub.infra.repo.opencsv;

import com.jawo.paymenthub.infra.repo.helper.RepoTestConfiguration;
import com.jawo.paymenthub.infra.repo.helper.RepoTestExtension;
import com.jawo.paymenthub.infra.repo.helper.RepoTestHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({ SpringExtension.class, RepoTestExtension.class })
@ContextConfiguration(classes = RepoTestConfiguration.class)
public class CsvPaymentRepositoryUnitTests {
    private CsvPaymentRepository paymentRepository;
    private AutoCloseable closeable;
    private CsvPaymentMapper paymentMapper;

    @BeforeEach
    public void setUpEach() {
        paymentMapper = new CsvPaymentMapper();
        paymentRepository = new CsvPaymentRepository(paymentMapper);
        paymentRepository.setDatafile("csv/payments_test.csv");
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }

    @Test
    public void findById_ShouldFind_Exists(RepoTestHelper repoTestHelper) {
        repoTestHelper.findById_ShouldFind_Exists(paymentRepository);
    }

    @Test
    public void findById_ShouldNotFind_NotExists(RepoTestHelper repoTestHelper) {
        repoTestHelper.findById_ShouldNotFind_NotExists(paymentRepository);
    }

    @Test
    public void findAll_ShouldFind_Exists(RepoTestHelper repoTestHelper) {
        repoTestHelper.findAll_ShouldFind_Exists(paymentRepository);
    }

    @Test
    public void findAll_ShouldNotFind_NotExists(RepoTestHelper repoTestHelper) {
        repoTestHelper.findAll_ShouldNotFind_NotExists(paymentRepository);
    }

    @Test
    public void create_ShouldCreate(RepoTestHelper repoTestHelper) {
        repoTestHelper.create_ShouldCreate(paymentRepository);
    }

    @Test
    public void create_ShouldCreate_CommaInUserId(RepoTestHelper repoTestHelper) {
        repoTestHelper.create_ShouldCreate_CommaInUserId(paymentRepository);
    }

    @Test
    public void create_ShouldCreate_EndOfLineInUserId(RepoTestHelper repoTestHelper) {
        repoTestHelper.create_ShouldCreate_EndOfLineInUserId(paymentRepository);
    }

    @Test
    public void update_ShouldUpdate_Exists(RepoTestHelper repoTestHelper) {
        repoTestHelper.update_ShouldUpdate_Exists(paymentRepository);
    }

    @Test
    public void update_ShouldNotUpdate_NotExists(RepoTestHelper repoTestHelper) {
        repoTestHelper.update_ShouldNotUpdate_NotExists(paymentRepository);
    }

    @Test
    public void delete_ShouldDelete_Exists(RepoTestHelper repoTestHelper) {
        repoTestHelper.delete_ShouldDelete_Exists(paymentRepository);
    }

    @Test
    public void delete_ShouldNotDelete_NotExists(RepoTestHelper repoTestHelper) {
        repoTestHelper.delete_ShouldNotDelete_NotExists(paymentRepository);
    }

    @Test
    public void deleteAll_ShouldDeleteAll(RepoTestHelper repoTestHelper) {
        repoTestHelper.deleteAll_ShouldDeleteAll(paymentRepository);
    }
}
