package com.jawo.paymenthub.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;

import java.io.FileReader;

public class TestUtils {
    public static String getResourceContent(String path) throws Exception{
        Resource messageContentFile = new ClassPathResource(path);
        return FileCopyUtils.copyToString(new FileReader(messageContentFile.getFile()));
    }
}
