package com.jawo.paymenthub.app;

import com.jawo.paymenthub.domain.Payment;
import com.jawo.paymenthub.domain.PaymentService;
import com.jawo.paymenthub.domain.exceptions.IllegalArgumentExceptionWithMap;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
public class PaymentControllerUnitTests {

    private PaymentService paymentService;
    private PaymentController paymentController;
    private AutoCloseable closeable;
    private AppPaymentMapper paymentMapper;

    private static Validator validator;

    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @BeforeEach
    public void setUpEach() {
        paymentService = mock(PaymentService.class);
        paymentMapper = new AppPaymentMapper();
        paymentController = new PaymentController(paymentService, paymentMapper);
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }

    @Test
    void findPaymentById_ShouldFindPayment_Exists() {
        //Given
        final Optional<Payment> payment = Optional.of(new Payment(1L, new BigDecimal(100.12), "PLN", "ikrasicki", "PL33109024024617497416165687"));
        when(paymentService.findPaymentById(1)).thenReturn(payment);

        //When
        PaymentDto paymentDto = paymentController.findPaymentById(1L);

        //Then
        verify(paymentService, times(1)).findPaymentById(any(Long.class));
        assertEquals(payment.get().getId(), paymentDto.getId());
        assertEquals(payment.get().getCcy(), paymentDto.getCcy());
        assertEquals(payment.get().getAmount(), new BigDecimal(paymentDto.getAmount()));
        assertEquals(payment.get().getAccountNo(), paymentDto.getAccountNo());
        assertEquals(payment.get().getUserId(), paymentDto.getUserId());
    }

    @Test
    void findPaymentById_ShouldNotFindPayment_NotExists() {
        //Given
        when(paymentService.findPaymentById(1)).thenReturn(Optional.empty());

        try {
            //When
            PaymentDto paymentDto = paymentController.findPaymentById(1L);

            //Then
            fail();
        } catch(ResponseStatusException e) {
            assertEquals(PaymentController.PAYMENT_NOT_FOUND, e.getReason());
            assertEquals(HttpStatus.NOT_FOUND.value(), e.getRawStatusCode());
        }
        verify(paymentService, times(1)).findPaymentById(any(Long.class));
    }

    @Test
    void findPaymentById_ShouldNotFindPayment_InvalidEmptyId() {
        try {
            //Given
            Object[] params = {null};

            //When
            Set<ConstraintViolation<PaymentController>> violations = validator.forExecutables()
                    .validateParameters(paymentController, paymentController.getClass().getMethod("findPaymentById", Long.class), params);

            //Then
            assertEquals(1, violations.size());
            verify(paymentService, times(0)).updatePayment(any());
        } catch(Exception e) {
            fail();
        }
    }

    @Test
    void findAllPayments_ShouldFindPayments_Exists() {
        //Given
        Payment payment = new Payment(1L, new BigDecimal(100.12), "PLN", "ikrasicki", "PL33109024024617497416165687");
        List<Payment> list = new ArrayList<>();
        list.add(payment);
        when(paymentService.findAllPayments()).thenReturn(list);

        //When
        List<PaymentDto> payments = paymentController.findAllPayments();

        //Then
        assertTrue(payments.size() == 1);
        verify(paymentService, times(1)).findAllPayments();
    }

    @Test
    void findAllPayments_ShouldNotFindPayments_NotExists() {
        //Given
        List<Payment> list = new ArrayList<>();
        when(paymentService.findAllPayments()).thenReturn(list);

        //When
        List<PaymentDto> payments = paymentController.findAllPayments();

        //Then
        assertTrue(payments.size() == 0);
        verify(paymentService, times(1)).findAllPayments();
    }

    @Test
    void createPayment_ShouldCreatePayment() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(null, "100.12", null, "ikrasicki", "PL33109024024617497416165687");
        final Payment payment = new Payment(1L, new BigDecimal(100.12), "PLN", "ikrasicki", "PL33109024024617497416165687");
        when(paymentService.createPayment(any(Payment.class))).thenReturn(payment);

        //When
        paymentController.createPayment(paymentDto);

        //Then
        verify(paymentService, times(1)).createPayment(any());
    }

    @Test
    void createPayment_ShouldNotCreatePayment_InvalidAmount() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(null, "abc.12", "PLN", "ikrasicki", "PL33109024024617497416165687");

        try {

            //When
            paymentController.createPayment(paymentDto);

            //Then
            fail();
        }  catch(IllegalArgumentExceptionWithMap e) {
            assertTrue(e.getMessages().containsKey("amount"));
        }
        verify(paymentService, times(0)).createPayment(any());
    }

    @Test
    void updatePayment_ShouldUpdatePayment_Exists() {
        //Given
        final PaymentDtoUpdate paymentDto = new PaymentDtoUpdate(1L, "100.12", "PLN", "ikrasicki", "PL33109024024617497416165687");
        final Optional<Payment> payment = Optional.of(paymentMapper.map(paymentDto));
        when(paymentService.updatePayment(any(Payment.class))).thenReturn(payment);

        //When
        paymentController.updatePayment(paymentDto);

        //Then
        verify(paymentService, times(1)).updatePayment(any());
    }

    @Test
    void updatePayment_ShouldUpdatePayment_NotExists() {
        //Given
        final PaymentDtoUpdate paymentDto = new PaymentDtoUpdate(1L, "100.12", "PLN", "ikrasicki", "PL33109024024617497416165687");
        try {
            when(paymentService.updatePayment(any(Payment.class))).thenReturn(Optional.empty());

            //When
            paymentController.updatePayment(paymentDto);

            //Then
            fail();
        } catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND.value(), e.getRawStatusCode());
        }
        verify(paymentService, times(1)).updatePayment(any());
    }

    @Test
    void updatePayment_ShouldNotUpdatePayment_InvalidAmount() {
        //Given
        final PaymentDtoUpdate paymentDto = new PaymentDtoUpdate(1L, "abc.12", "PLN", "ikrasicki", "PL33109024024617497416165687");

        try {
            //When
            paymentController.updatePayment(paymentDto);

            //Then
            fail();
        } catch(IllegalArgumentExceptionWithMap e) {
            assertTrue(e.getMessages().containsKey("amount"));
        }

        verify(paymentService, times(0)).updatePayment(any());
    }


    @Test
    void deletePayment_ShouldDeletePayment() {
        //Given
        when(paymentService.deletePayment(any(Long.class))).thenReturn(true);

        //When
        paymentController.deletePayment(1L);

        //Then
        verify(paymentService, times(1)).deletePayment(any(Long.class));
    }

    @Test
    void deletePayment_ShouldNotDeletePayment() {
        //Given
        when(paymentService.deletePayment(any(Long.class))).thenReturn(false);

        //When
        try {
            paymentController.deletePayment(1L);

            //Then
            fail();
        } catch(ResponseStatusException e) {
            assertEquals(HttpStatus.NOT_FOUND.value(), e.getRawStatusCode());
        }

        verify(paymentService, times(1)).deletePayment(any(Long.class));
    }
}
