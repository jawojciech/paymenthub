package com.jawo.paymenthub.app;

import com.jawo.paymenthub.domain.exceptions.IllegalArgumentExceptionWithMap;
import com.jawo.paymenthub.domain.validators.PaymentValidation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class PaymentDtoUpdateUnitTests {
    private static Validator validator;

    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void newPayment_ShouldCreate() {
        //Given
        try {
            //When
            final PaymentDtoUpdate paymentDto = new PaymentDtoUpdate(1L, "100.12", "PLN", "ikrasicki", "PL33109024024617497416165687");

            //Then
        } catch(Exception e) {
            fail();
        }
    }

    @Test
    void newPayment_ShouldNotCreate() {
        //Given
        try {
            //When
            final PaymentDtoUpdate paymentDto = new PaymentDtoUpdate(null, "100.12", "PLN", "ikrasicki", "PL33109024024617497416165687");

            //Then
            fail();
        } catch(IllegalArgumentExceptionWithMap e) {
            assertTrue(e.getMessages().get(PaymentDtoUpdate.PAYMENT_ID).contains(PaymentDtoUpdate.PAYMENT_ID_REQUIRED));
        }
    }
}
