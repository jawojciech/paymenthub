package com.jawo.paymenthub.app;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentDtoUnitTests {
    private static Validator validator;

    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void newPayment_ShouldCreate() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);

        //Then
        assertTrue(violations.isEmpty());
    }

    @Test
    void newPaymentDto_ShouldNotCreate_InvalidIdNotPositive() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(-1L, "100.12", "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAmountNull() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, null, "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAmountBlank() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "  ", "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyNull() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", null, "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyEmpty() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyBlank() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "   ", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyToShort() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PL", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidCcyToLong() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLNO", "ikrasicki", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidUserIdNull() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", null, "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidUserIdEmpty() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", "", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidUserIdBlank() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", "  ", "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidUserIdToLong() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", RandomStringUtils.randomAlphabetic(65), "PL33109024024617497416165687");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAccountNoNull() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", "ikrasicki", null);

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAccountNoEmpty() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", "ikrasicki", "");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(2, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAccountNoBlank() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", "ikrasicki", "  ");

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }

    @Test
    void newPayment_ShouldNotCreate_InvalidAccountNoToLong() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", "ikrasicki", RandomStringUtils.randomAlphabetic(35));

        //When
        Set<ConstraintViolation<PaymentDto>> violations = validator.validate(paymentDto);
        assertEquals(1, violations.size());
    }
}
