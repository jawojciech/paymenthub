package com.jawo.paymenthub.app;

import com.jawo.paymenthub.domain.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AppPaymentMapperUnitTests {
    AppPaymentMapper appPaymentMapper;

    @BeforeEach
    void setUp() {
        appPaymentMapper = new AppPaymentMapper();
    }

    @Test
    void map_shouldMapDomain2Dummy() {
        //Given
        final Payment payment = new Payment(1L, new BigDecimal(100), "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        PaymentDto paymentDto = appPaymentMapper.map(payment);

        //Then
        assertEquals(payment.getId(), paymentDto.getId());
        assertEquals(payment.getAmount(), new BigDecimal(paymentDto.getAmount()));
        assertEquals(payment.getAccountNo(), paymentDto.getAccountNo());
        assertEquals(payment.getCcy(), paymentDto.getCcy());
        assertEquals(payment.getUserId(), paymentDto.getUserId());
    }

    @Test
    void map_shouldMapDummy2Domain() {
        //Given
        final PaymentDto paymentDto = new PaymentDto(1L, "100.12", "PLN", "ikrasicki", "PL33109024024617497416165687");

        //When
        Payment payment = appPaymentMapper.map(paymentDto);

        //Then
        assertEquals(payment.getId(), paymentDto.getId());
        assertEquals(payment.getAmount(), new BigDecimal(paymentDto.getAmount()));
        assertEquals(payment.getAccountNo(), paymentDto.getAccountNo());
        assertEquals(payment.getCcy(), paymentDto.getCcy());
        assertEquals(payment.getUserId(), paymentDto.getUserId());
    }
}
