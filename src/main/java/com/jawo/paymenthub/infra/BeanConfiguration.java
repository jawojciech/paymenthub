package com.jawo.paymenthub.infra;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.jawo.paymenthub.domain.DomainPaymentService;
import com.jawo.paymenthub.domain.PaymentRepository;
import com.jawo.paymenthub.domain.PaymentService;

@Configuration
public class BeanConfiguration {
    @Bean
    PaymentService paymentService(PaymentRepository paymentRepository) {
        return new DomainPaymentService(paymentRepository);
    }
}
