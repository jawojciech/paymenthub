package com.jawo.paymenthub.infra.repo.opencsv;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.Reader;

public class CsvReaderAuto extends CSVReader implements AutoCloseable {

    public CsvReaderAuto(Reader reader) {
        super(reader);
    }

    @Override
    public void close() throws IOException {
        super.close();
    }
}
