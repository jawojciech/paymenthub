package com.jawo.paymenthub.infra.repo.dummy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import com.jawo.paymenthub.domain.Payment;
import com.jawo.paymenthub.domain.PaymentRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
@ConditionalOnProperty(
        value="payment.hub.repository.provider",
        havingValue = "DUMMY")
public class DummyPaymentRepository implements PaymentRepository {
    private static final Logger logger = LogManager.getLogger();
    @Value("${payment.hub.repository.provider}")
    private String repositoryType;
    private static Hashtable<Long, DummyPayment> payments = new Hashtable<>();
    private DummyPaymentMapper paymentMapper;
    private final static AtomicLong counter = new AtomicLong();

    @PostConstruct
    public void init() {
        logger.info("Started with repository provider set to {}", repositoryType);
    }

    @Autowired
    public DummyPaymentRepository(DummyPaymentMapper paymentMapper) {
        this.paymentMapper = paymentMapper;
    }

    @Override
    public Optional<Payment> findById(long id) {
        return Optional.ofNullable(paymentMapper.map(payments.get(id)));
    }

    @Override
    public List<Payment> findAll() {
        //first copy, otherwise concurrency issues with stream
        List<DummyPayment> result = new ArrayList<>(payments.values());
        return result.stream().map(dummyPayment -> paymentMapper.map(dummyPayment)).collect(Collectors.toList());
    }

    @Override
    public Payment create(Payment payment){
        DummyPayment dummyPayment = paymentMapper.map(payment);

        dummyPayment.setId(generateNextId());
        payments.put(dummyPayment.getId(), dummyPayment);

        return paymentMapper.map(dummyPayment);
    }

    @Override
    public Optional<Payment> update(Payment payment) {
        DummyPayment dummyPayment2Update = payments.get(payment.getId());

        if (dummyPayment2Update != null) {
            DummyPayment newDummyPayment = paymentMapper.map(payment);
            dummyPayment2Update.setAmount(newDummyPayment.getAmount());
            dummyPayment2Update.setAccountNo(newDummyPayment.getAccountNo());
            dummyPayment2Update.setCcy(newDummyPayment.getCcy());
            dummyPayment2Update.setUserId(newDummyPayment.getUserId());

            return Optional.of(paymentMapper.map(dummyPayment2Update));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean deleteById(long paymentId) {
        return payments.remove(paymentId) != null ? true : false;
    }

    @Override
    public void deleteAllPayments() {
        payments.clear();
    }

    private static long generateNextId() {
        return counter.incrementAndGet();
    }
}
