package com.jawo.paymenthub.infra.repo.jpa;

import com.jawo.paymenthub.domain.Payment;
import com.jawo.paymenthub.domain.PaymentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
@ConditionalOnProperty(
        value="payment.hub.repository.provider",
        havingValue = "JPA")
public class JpaPaymentRepositoryAdapter implements PaymentRepository {
    private static final Logger logger = LogManager.getLogger();
    @Value("${payment.hub.repository.provider}")
    private String repositoryType;
    private JpaPaymentMapper paymentMapper;
    private final JpaPaymentRepository jpaPaymentRepository;

    @PostConstruct
    public void init() {
        logger.info("Started with repository provider set to {}", repositoryType);
    }

    @Autowired
    public JpaPaymentRepositoryAdapter(JpaPaymentMapper paymentMapper, JpaPaymentRepository jpaPaymentRepository) {
        this.paymentMapper = paymentMapper;
        this.jpaPaymentRepository = jpaPaymentRepository;
    }

    @Override
    public Optional<Payment> findById(long id) {
        Optional<JpaPayment> jpaPayment = jpaPaymentRepository.findById(id);

        return jpaPayment.map(paymentMapper::map);
    }

    @Override
    public List<Payment> findAll() {
        return StreamSupport.stream(jpaPaymentRepository.findAll().spliterator(), false)
                .map(jpaPayment -> paymentMapper.map(jpaPayment)).collect(Collectors.toList());
    }

    @Override
    public Payment create(Payment payment){
        JpaPayment jpaPayment = paymentMapper.map(payment);

        return paymentMapper.map(jpaPaymentRepository.save(jpaPayment));
    }

    @Override
    public Optional<Payment> update(Payment payment) {

        Optional<JpaPayment> jpaPayment = jpaPaymentRepository.findById(payment.getId());

        if (jpaPayment.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(paymentMapper.map(jpaPaymentRepository.save(paymentMapper.map(payment))));
    }

    @Override
    public boolean deleteById(long paymentId) {
        try {
            jpaPaymentRepository.deleteById(paymentId);

            return true;
        } catch(EmptyResultDataAccessException e) {
            return false;
        }
    }

    @Override
    public void deleteAllPayments() {
        jpaPaymentRepository.deleteAll();
    }
}
