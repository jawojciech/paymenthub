package com.jawo.paymenthub.infra.repo.jpa;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "payments")
public class JpaPayment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name="amount")
    BigDecimal amount;
    @Column(name="ccy", nullable = false, length = 3)
    String ccy;
    @Column(name="userId", nullable = false, length = 32)
    String userId;
    @Column(name="accountNo", nullable = false, length = 34)
    String accountNo;

    public JpaPayment() { }

    public JpaPayment(BigDecimal amount, String ccy, String userId, String accountNo) {
        this.amount = amount;
        this.ccy = ccy;
        this.userId = userId;
        this.accountNo = accountNo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
}
