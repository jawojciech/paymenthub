package com.jawo.paymenthub.infra.repo.jpa;

import com.jawo.paymenthub.domain.Payment;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class JpaPaymentMapper {
    public Payment map(JpaPayment jpaPayment) {
        if (jpaPayment == null) {
            return null;
        }
        Payment payment = new Payment();
        try {
            BeanUtils.copyProperties(jpaPayment, payment);
            return payment;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public JpaPayment map(Payment payment) {
        if (payment == null) {
            return null;
        }

        JpaPayment jpaPayment = new JpaPayment();
        try {
            if (payment.getId() != null) {
                jpaPayment.setId(payment.getId());
            }
            jpaPayment.setCcy(payment.getCcy());
            jpaPayment.setAmount(payment.getAmount());
            jpaPayment.setUserId(payment.getUserId());
            jpaPayment.setAccountNo(payment.getAccountNo());
            return jpaPayment;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
