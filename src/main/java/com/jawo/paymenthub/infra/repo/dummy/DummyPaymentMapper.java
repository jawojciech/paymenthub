package com.jawo.paymenthub.infra.repo.dummy;

import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import com.jawo.paymenthub.domain.Payment;

@Component
@ConditionalOnProperty(
        value="payment.hub.repository.provider",
        havingValue = "DUMMY")
public class DummyPaymentMapper {
    public Payment map(DummyPayment dummyPayment) {
        if (dummyPayment == null) {
            return null;
        }
        Payment payment = new Payment();
        try {
            BeanUtils.copyProperties(dummyPayment, payment);
            return payment;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public DummyPayment map(Payment payment) {
        if (payment == null) {
            return null;
        }

        DummyPayment dummyPayment = new DummyPayment();
        try {
            BeanUtils.copyProperties(payment, dummyPayment);
            return dummyPayment;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
