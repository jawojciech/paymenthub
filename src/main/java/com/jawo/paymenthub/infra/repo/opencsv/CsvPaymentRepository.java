package com.jawo.paymenthub.infra.repo.opencsv;

import com.jawo.paymenthub.domain.Payment;
import com.jawo.paymenthub.domain.PaymentRepository;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@ConditionalOnProperty(
        value="payment.hub.repository.provider",
        havingValue = "CSV")
public class CsvPaymentRepository implements PaymentRepository {
    private static final Logger logger = LogManager.getLogger();
    @Value("${payment.hub.repository.provider}")
    private String repositoryType;
    @Value("${payment.hub.csv.datafile}")
    private String datafile;
    private CsvPaymentMapper paymentMapper;

    @PostConstruct
    public void init() {
        logger.info("Started with repository provider set to {}", repositoryType);
    }

    @Autowired
    public CsvPaymentRepository(CsvPaymentMapper paymentMapper) {
        this.paymentMapper = paymentMapper;
    }

    @Override
    public Optional<Payment> findById(long id) {
        synchronized(this) {
            CSVReader csvReader = null;
            try {
                csvReader = new CSVReader(getReader());
                String[] line;

                while ((line = csvReader.readNext()) != null) {
                    if (line[CsvFieldPosition.ID.ordinal()] != null &&
                            Integer.parseInt(line[CsvFieldPosition.ID.ordinal()]) == id) {
                        return Optional.of(paymentMapper.map(line));
                    }
                }

                return Optional.empty();
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                if (csvReader != null) {
                    try {
                        csvReader.close();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }

    @Override
    public List<Payment> findAll() {
        synchronized(this) {
            try {
                return readAll().stream().map(paymentDto -> paymentMapper.map(paymentDto))
                        .collect(Collectors.toList());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public Payment create(Payment payment){
        synchronized(this) {
            Pair<Long, List<String[]>> current;
            try (CsvReaderAuto csvReaderAuto = new CsvReaderAuto(getReader())) {
                current = readAllAndGenerateNextId(csvReaderAuto);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            payment.setId(current.getLeft());
            current.getRight().add(paymentMapper.map(payment));

            /*
                it seems that Open CSV library doesn't support append operation - so it is rewrite of a file
            */
            try (CsvWriterAuto csvWriterAuto = new CsvWriterAuto(getWriter())) {
                csvWriterAuto.writeAll(current.getRight());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            return payment;
        }
    }

    @Override
    public Optional<Payment> update(Payment payment) {
        synchronized(this) {
            List<String[]> current;
            try {
                current = readAll();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            Payment result = null;
            for(String[] line : current) {
                if (Integer.parseInt(line[0]) == payment.getId()) {
                    result = paymentMapper.set(payment, line);
                    break;
                }
            }

            if (result == null) {
                return Optional.empty();
            }
            try (CsvWriterAuto csvWriterAuto = new CsvWriterAuto(getWriter())) {
                csvWriterAuto.writeAll(current);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            return Optional.of(result);
        }
    }

    @Override
    public boolean deleteById(long paymentId) {
        boolean result = false;
        synchronized(this) {
            List<String[]> current;
            try {
                current = readAll();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            for(String[] line : current) {
                if (Integer.parseInt(line[0]) == paymentId) {
                    current.remove(line);
                    result = true;
                    break;
                }
            }

            try (CsvWriterAuto csvWriterAuto = new CsvWriterAuto(getWriter())) {
                csvWriterAuto.writeAll(current);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return result;
    }

    @Override
    public void deleteAllPayments() {
        try(CsvWriterAuto csvWriterAuto = new CsvWriterAuto(getWriter())) {
            csvWriterAuto.writeAll(new ArrayList<>());
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<String[]> readAll() throws Exception {
        try(CsvReaderAuto csvReaderAuto = new CsvReaderAuto(getReader())) {
            return csvReaderAuto.readAll();
        }
    }

    public void setDatafile(String datafile) {
        this.datafile = datafile;
    }

    private Pair<Long, List<String[]>> readAllAndGenerateNextId(CSVReader csvReader) throws Exception {
        String[] line;
        long maxId = 0;
        List<String[]> lines = new LinkedList<>();

        while ((line = csvReader.readNext()) != null) {
            lines.add(line);
            if (line[CsvFieldPosition.ID.ordinal()] != null) {
                int current = Integer.parseInt(line[CsvFieldPosition.ID.ordinal()]);
                if (current > maxId){
                    maxId = current;
                }
            }
        }

        return new ImmutablePair<>(++maxId, lines);
    }

    private Reader getReader() throws Exception {
        return Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource(datafile).toURI()));
    }

    private Writer getWriter() throws Exception {
        return Files.newBufferedWriter(Paths.get(ClassLoader.getSystemResource(datafile).toURI()));
    }
}
