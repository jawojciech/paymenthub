package com.jawo.paymenthub.infra.repo.jpa;
import org.springframework.data.repository.CrudRepository;

public interface JpaPaymentRepository extends CrudRepository<JpaPayment, Long>  {

}
