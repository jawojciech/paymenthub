package com.jawo.paymenthub.infra.repo.opencsv;

import com.opencsv.CSVWriter;

import java.io.IOException;
import java.io.Writer;

public class CsvWriterAuto extends CSVWriter implements AutoCloseable {

    public CsvWriterAuto(Writer writer) {
        super(writer);
    }

    @Override
    public void close() throws IOException {
        super.close();
    }
}
