package com.jawo.paymenthub.infra.repo.dummy;

import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DummyPayment {
    private Long id;
    private BigDecimal amount;
    private String ccy;
    private String userId;
    private String accountNo;
}
