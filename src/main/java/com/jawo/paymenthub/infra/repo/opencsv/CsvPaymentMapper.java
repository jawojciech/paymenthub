package com.jawo.paymenthub.infra.repo.opencsv;

import com.jawo.paymenthub.domain.Payment;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@ConditionalOnProperty(
        value="payment.hub.repository.provider",
        havingValue = "CSV")
public class CsvPaymentMapper {
    public Payment map(String[] paymentDto) {
        if (paymentDto == null) {
            return null;
        }
        Payment payment = new Payment();
        try {
            payment.setId(Long.parseLong(paymentDto[CsvFieldPosition.ID.ordinal()]));
            payment.setAmount(new BigDecimal(paymentDto[CsvFieldPosition.AMOUNT.ordinal()]));
            payment.setCcy(paymentDto[CsvFieldPosition.CCY.ordinal()]);
            payment.setUserId(paymentDto[CsvFieldPosition.USER_ID.ordinal()]);
            payment.setAccountNo(paymentDto[CsvFieldPosition.ACCOUNT_NO.ordinal()]);

            return payment;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String[] map(Payment payment) {
        if (payment == null) {
            return null;
        }

        String[] paymentDto = new String[] { payment.getId().toString(), payment.getAmount().toString()
                ,payment.getCcy(), payment.getUserId(), payment.getAccountNo()};
        return paymentDto;
    }

    public Payment set(Payment payment, String[] paymentDto) {
        paymentDto[CsvFieldPosition.AMOUNT.ordinal()] = payment.getAmount().toString();
        paymentDto[CsvFieldPosition.CCY.ordinal()] = payment.getCcy();
        paymentDto[CsvFieldPosition.USER_ID.ordinal()] = payment.getUserId();
        paymentDto[CsvFieldPosition.ACCOUNT_NO.ordinal()] = payment.getAccountNo();

        return payment;
    }
}
