package com.jawo.paymenthub.infra.repo.opencsv;

public enum CsvFieldPosition {
    ID, AMOUNT, CCY, USER_ID, ACCOUNT_NO
}
