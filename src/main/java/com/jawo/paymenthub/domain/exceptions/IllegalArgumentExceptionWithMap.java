package com.jawo.paymenthub.domain.exceptions;

import java.util.Map;

public class IllegalArgumentExceptionWithMap extends IllegalArgumentException {
    private Map<String, String> messages;

    public IllegalArgumentExceptionWithMap(Map<String, String> messages) {
        this.messages = messages;
    }

    public Map<String, String> getMessages() {
        return messages;
    }
}
