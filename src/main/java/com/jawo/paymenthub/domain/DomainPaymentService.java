package com.jawo.paymenthub.domain;

import com.jawo.paymenthub.domain.exceptions.IllegalArgumentExceptionWithMap;
import com.jawo.paymenthub.domain.validators.BasicPaymentValidation;
import com.jawo.paymenthub.domain.validators.PaymentValidation;
import com.jawo.paymenthub.domain.validators.PaymentValidator;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.jawo.paymenthub.domain.validators.PaymentValidator.validateUserId;
import static com.jawo.paymenthub.domain.validators.PaymentValidator.validateAccount;

public class DomainPaymentService implements PaymentService{
    private final PaymentRepository paymentRepository;

    public DomainPaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Override
    public Optional<Payment> findPaymentById(long id) {
        return paymentRepository.findById(id);
    }

    @Override
    public List<Payment> findAllPayments() {
        return paymentRepository.findAll();
    }

    @Override
    public Payment createPayment(Payment payment) {
        return paymentRepository.create(validatePayment(payment));
    }

    @Override
    public Optional<Payment> updatePayment(Payment payment) {
        return paymentRepository.update(validatePayment(payment));
    }

    @Override
    public boolean deletePayment(long paymentId) {
        return paymentRepository.deleteById(paymentId);
    }

    public Payment validatePayment(Payment payment) {
        final Function<PaymentValidation, PaymentValidation> combineValidators =
                PaymentValidator.validateCcy().andThen(validateUserId()).andThen(validateAccount());

        PaymentValidation paymentValidation = combineValidators.apply(new BasicPaymentValidation(payment));

        if (paymentValidation.isInvalid()) {
            throw new IllegalArgumentExceptionWithMap(paymentValidation.getMessages());
        }

        return payment;
    }
}
