package com.jawo.paymenthub.domain;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class Payment {
    @Positive
    private Long id;

    @NotNull
    @Positive
    private BigDecimal amount;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 3)
    private String ccy;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 32)
    private String userId;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 34)
    private String accountNo;
}
