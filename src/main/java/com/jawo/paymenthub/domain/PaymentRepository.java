package com.jawo.paymenthub.domain;

import java.util.List;
import java.util.Optional;

public interface PaymentRepository {
    Optional<Payment> findById(long id);

    List<Payment> findAll();

    Payment create(Payment payment);

    Optional<Payment> update(Payment payment);

    boolean deleteById(long id);

    void deleteAllPayments();
}
