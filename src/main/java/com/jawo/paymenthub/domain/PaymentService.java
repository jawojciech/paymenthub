package com.jawo.paymenthub.domain;

import java.util.List;
import java.util.Optional;

public interface PaymentService {
    Optional<Payment> findPaymentById(long id);

    List<Payment> findAllPayments();

    Payment createPayment(Payment payment);

    Optional<Payment> updatePayment(Payment payment);

    boolean deletePayment(long paymentId);
}
