package com.jawo.paymenthub.domain.validators;

import com.jawo.paymenthub.domain.Payment;

import java.util.HashMap;
import java.util.Map;

public class BasicPaymentValidation implements PaymentValidation {
    public static final String INVALID_CURRENCY = "currency value required in ISO 4217 format";
    public static final String INVALID_USER_ID = "user id can consists of alphanumeric characters only";
    public static final String INVALID_ACCOUNT = "account number required in IBAN format";

    private boolean isValid;
    private Map<String, String> messages;
    private final Payment payment;

    public BasicPaymentValidation(Payment payment) {
        this.payment = payment;
        this.isValid = true;
        this.messages = new HashMap<>();
    }

    @Override
    public boolean isInvalid() {
        return !isValid;
    }

    @Override
    public Map<String, String> getMessages() {
        return messages;
    }

    /* Below validations could be done with @Pattern validation annotation in Payment
       but for the sake of the exercise they are done here.
       In real live scenario they would more complicated anyway.
     */
    @Override
    public PaymentValidation validateCcy() {
        if(!payment.getCcy().matches("^[A-Z]{3}")) {
            isValid = false;
            messages.put(CCY, INVALID_CURRENCY);
        }
        return this;
    }

    @Override
    public PaymentValidation validateUserId() {
        if(!payment.getUserId().matches("^[a-zA-Z0-9]+$")) {
            isValid = false;
            messages.put(USER_ID, INVALID_USER_ID);
        }
        return this;
    }

    @Override
    public PaymentValidation validateAccountNo() {
        if(!payment.getAccountNo().matches("^[A-Z]{2}[0-9].*")) {
            isValid = false;
            messages.put(ACCOUNT_NO, INVALID_ACCOUNT);
        }

        return this;
    }
}
