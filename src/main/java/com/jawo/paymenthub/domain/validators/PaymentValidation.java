package com.jawo.paymenthub.domain.validators;

import java.util.Map;

public interface PaymentValidation {
    public static final String USER_ID = "userId";
    public static final String CCY = "ccy";
    public static final String ACCOUNT_NO = "accountNo";

    boolean isInvalid();
    Map<String, String> getMessages();

    PaymentValidation validateCcy();

    PaymentValidation validateUserId();

    PaymentValidation validateAccountNo();
}
