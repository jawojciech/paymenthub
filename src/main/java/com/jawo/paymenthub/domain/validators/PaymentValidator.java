package com.jawo.paymenthub.domain.validators;

import java.util.function.UnaryOperator;

public interface PaymentValidator extends UnaryOperator<PaymentValidation> {

    default PaymentValidator combine(PaymentValidator after) {
        return value -> after.apply(this.apply(value));
    }

    static PaymentValidator validateCcy() {
        return (paymentValidation) -> paymentValidation.validateCcy();
    }

    static PaymentValidator validateUserId() {
        return (paymentValidation) -> paymentValidation.validateUserId();
    }

    static PaymentValidator validateAccount() {
        return (paymentValidation) -> paymentValidation.validateAccountNo();
    }
}
