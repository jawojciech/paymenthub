package com.jawo.paymenthub.app;

import com.jawo.paymenthub.domain.exceptions.IllegalArgumentExceptionWithMap;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import lombok.*;

import com.jawo.paymenthub.domain.PaymentService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/payments")
@AllArgsConstructor
public class PaymentController {
    static String PAYMENT_ID_REQUIRED = "Payment id is required";
    static String PAYMENT_NOT_FOUND = "Payment not found";
    static String INVALID_ARGUMENTS = "Invalid arguments";

    private final PaymentService paymentService;
    private final AppPaymentMapper paymentMapper;

    @GetMapping("{paymentId}")
    public PaymentDto findPaymentById(@Valid @NotNull @Positive @PathVariable Long paymentId) {
        return paymentService.findPaymentById(paymentId)
            .map(paymentMapper::map)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, PAYMENT_NOT_FOUND));
    }

    @GetMapping()
    public List<PaymentDto> findAllPayments() {
        return paymentService.findAllPayments().stream().map(payment -> paymentMapper.map(payment))
                .collect(Collectors.toList());
    }

    @PostMapping()
    public PaymentDto createPayment(@Valid @RequestBody PaymentDto paymentDto) {
        return paymentMapper.map(paymentService.createPayment(paymentMapper.map(paymentDto)));
    }

    @PutMapping()
    public PaymentDto updatePayment(@Valid @RequestBody PaymentDtoUpdate paymentDto) {
        return paymentService.updatePayment(paymentMapper.map(paymentDto)).map(paymentMapper::map)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, PAYMENT_NOT_FOUND));
    }

    @DeleteMapping("{paymentId}")
    public void deletePayment(@PathVariable Long paymentId) {
        if (!paymentService.deletePayment(paymentId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, PAYMENT_NOT_FOUND);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentExceptionWithMap.class)
    public Map<String, String>  handleIllegalArgumentExceptions(IllegalArgumentExceptionWithMap ex) {
       return ex.getMessages();
    }
}
