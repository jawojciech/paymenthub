package com.jawo.paymenthub.app;

import com.jawo.paymenthub.domain.exceptions.IllegalArgumentExceptionWithMap;
import org.springframework.stereotype.Component;
import com.jawo.paymenthub.domain.Payment;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Component
public class AppPaymentMapper {
    public Payment map(PaymentDto paymentDto) {
        if (paymentDto == null) {
            return null;
        }
        Payment payment = new Payment();
        try {
            payment.setAmount(new BigDecimal(paymentDto.getAmount()));
        } catch (NumberFormatException e) {
            Map<String, String> err = new HashMap<>();
            err.put("amount", e.getMessage());
            throw new IllegalArgumentExceptionWithMap(err);
        }
        payment.setId(paymentDto.getId());
        payment.setAccountNo(paymentDto.getAccountNo());
        payment.setCcy(paymentDto.getCcy());
        payment.setUserId(paymentDto.getUserId());
        return payment;
    }

    public PaymentDto map(Payment payment) {
        if (payment == null) {
            return null;
        }

        PaymentDto paymentDto = new PaymentDto();

        paymentDto.setId(payment.getId());
        paymentDto.setAmount(payment.getAmount().toString());
        paymentDto.setAccountNo(payment.getAccountNo());
        paymentDto.setCcy(payment.getCcy());
        paymentDto.setUserId(payment.getUserId());

        return paymentDto;
    }
}
