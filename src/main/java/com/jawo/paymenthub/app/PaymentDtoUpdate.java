package com.jawo.paymenthub.app;

import com.jawo.paymenthub.domain.exceptions.IllegalArgumentExceptionWithMap;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;

public class PaymentDtoUpdate extends PaymentDto {
    public final static String PAYMENT_ID = "id";
    public final static String PAYMENT_ID_REQUIRED = "Payment id is required for update operation";

    public PaymentDtoUpdate(@Positive Long id, @NotNull @NotBlank String amount, @NotNull @NotBlank @Size(min = 3, max = 3) String ccy, @NotNull @NotBlank @Size(min = 1, max = 32) String userId, @NotNull @NotBlank @Size(min =1, max = 34) String accountNo) {
        setId(id);
        this.amount = amount;
        this.ccy = ccy;
        this.userId = userId;
        this.accountNo = accountNo;
    }

    @Override
    public void setId(Long id) {
        if (id == null) {
            Map<String, String> err = new HashMap<>();
            err.put("id", PAYMENT_ID_REQUIRED);
            throw new IllegalArgumentExceptionWithMap(err);
        }

        super.setId(id);
    }
}
