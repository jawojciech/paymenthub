package com.jawo.paymenthub.app;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PaymentDto {
    @Positive
    protected Long id;

    @NotNull
    @NotBlank
    protected String amount;

    @NotNull
    @NotBlank
    @Size(min = 3, max = 3)
    protected String ccy;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 32)
    protected String userId;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 34)
    protected String accountNo;
}
