package com.jawo.paymenthub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
//@SpringBootApplication
public class PaymentHubApplication {



	public static void main(String[] args) {
		SpringApplication.run(PaymentHubApplication.class, args);
	}

}
