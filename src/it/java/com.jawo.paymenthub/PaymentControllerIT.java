package com.jawo.paymenthub;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jawo.paymenthub.app.PaymentDto;
import com.jawo.paymenthub.app.PaymentDtoUpdate;
import com.jawo.paymenthub.domain.validators.BasicPaymentValidation;
import com.jawo.paymenthub.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(properties = { "payment.hub.csv.datafile=csv/payments_it.csv" })
@AutoConfigureMockMvc
public class PaymentControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void findPaymentById_ShouldFindPayment_Exists() throws Exception {
        //Given
        PaymentDto paymentDto = createPayment("data/newValidPayment1.json");

        //When
        ResultActions resultActions = mockMvc.perform(get("/payments/{id}", paymentDto.getId()));

        //Then
        resultActions
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(paymentDto.getId()), Long.class))
                .andExpect(jsonPath("$.amount", is(paymentDto.getAmount())))
                .andExpect(jsonPath("$.ccy", is(paymentDto.getCcy())))
                .andExpect(jsonPath("$.userId", is(paymentDto.getUserId())))
                .andExpect(jsonPath("$.accountNo", is(paymentDto.getAccountNo())));
    }

    @Test
    void findPaymentById_ShouldNotFindPayment_NotExists() throws Exception {
        //Given
        long paymentId = 10L;
        deletePayment(paymentId);

        //When
        ResultActions resultActions = mockMvc.perform(get("/payments/{id}", paymentId));

        //Then
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    void findAllPayments_ShouldFindAllPayments_Exists() throws Exception {
        //Given
        createPayment("data/newValidPayment1.json");

        //When
        ResultActions resultActions = mockMvc.perform(get("/payments"));

        //Then
        resultActions
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThan(0))));

    }

    @Test
    void createPayment_ShouldCreatePayment() throws Exception {
        //Given
        String payload = TestUtils.getResourceContent("data/newValidPayment1.json");

        //When
        ResultActions resultActions = mockMvc.perform(post("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.userId", is("jrybak")));
    }

    @Test
    void createPayment_ShouldNotCreatePayment_InvalidAmount() throws Exception {
        //Given
        String payload = TestUtils.getResourceContent("data/newInvalidPaymentInvalidAmount.json");

        //When
        ResultActions resultActions = mockMvc.perform(post("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.amount", is("Character a is neither a decimal digit number, decimal point, nor \"e\" notation exponential mark.")));
    }

    @Test
    void createPayment_ShouldNotCreatePayment_InvalidCcy() throws Exception {
        //Given
        String payload = TestUtils.getResourceContent("data/newInvalidPaymentInvalidCcy.json");

        //When
        ResultActions resultActions = mockMvc.perform(post("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ccy", is(BasicPaymentValidation.INVALID_CURRENCY)));
    }

    @Test
    void createPayment_ShouldNotCreatePayment_InvalidCcyEmpty() throws Exception {
        //Given
        String payload = TestUtils.getResourceContent("data/newInvalidPaymentInvalidCcyEmpty.json");

        //When
        ResultActions resultActions = mockMvc.perform(post("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
                //.andExpect(jsonPath("$.ccy", is("must not be blank" )));
    }

    //is(!toString().isEmpty()

    @Test
    void createPayment_ShouldNotCreatePayment_InvalidCcyAndAccount() throws Exception {
        //Given
        String payload = TestUtils.getResourceContent("data/newInvalidPaymentInvalidCcyAndAccount.json");

        //When
        ResultActions resultActions = mockMvc.perform(post("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.accountNo", is(BasicPaymentValidation.INVALID_ACCOUNT)))
                .andExpect(jsonPath("$.ccy", is(BasicPaymentValidation.INVALID_CURRENCY)));
    }

    @Test
    void createPayment_ShouldNotCreatePayment_InvalidAccount() throws Exception {
        //Given
        String payload = TestUtils.getResourceContent("data/newInvalidPaymentInvalidAccount.json");

        //When
        ResultActions resultActions = mockMvc.perform(post("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.accountNo", is(BasicPaymentValidation.INVALID_ACCOUNT)));
    }

    @Test
    void updatePayment_ShouldUpdatePayment_Exists() throws Exception {
        //Given
        String testFile = "data/newValidPayment1.json";
        PaymentDto paymentDto = createPayment(testFile);
        paymentDto.setUserId("newUserId");
        String payload = objectMapper.writeValueAsString(paymentDto);

        //When
        ResultActions resultActions = mockMvc.perform(put("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(paymentDto.getId()), Long.class))
                .andExpect(jsonPath("$.userId", is(paymentDto.getUserId())));
    }

    @Test
    void updatePayment_ShouldNotUpdatePayment_InvalidIdNull() throws Exception {
        //Given
        String payload = TestUtils.getResourceContent("data/updInvalidPaymentInvalidIdNull.json");

        //When
        ResultActions resultActions = mockMvc.perform(put("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$." + PaymentDtoUpdate.PAYMENT_ID, is(PaymentDtoUpdate.PAYMENT_ID_REQUIRED)));
    }

    @Test
    void updatePayment_ShouldNotUpdatePayment_InvalidAmount() throws Exception {
        //Given
        String payload = TestUtils.getResourceContent("data/updInvalidPaymentInvalidAmount.json");

        //When
        ResultActions resultActions = mockMvc.perform(put("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.amount", is("Character a is neither a decimal digit number, decimal point, nor \"e\" notation exponential mark.")));
    }

    @Test
    void deletePayment_ShouldDeletePayment_Exists() throws Exception {
        //Given
        String testFile = "data/newValidPayment1.json";
        PaymentDto paymentDto = createPayment(testFile);

        //When
        ResultActions resultActions = mockMvc.perform(delete("/payments/{id}", paymentDto.getId())
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isOk());
    }

    @Test
    void deletePayment_ShouldNotDeletePayment_NotExists() throws Exception {
        //Given
        long paymentId = 10L;
        deletePayment(paymentId);

        //When
        ResultActions resultActions = mockMvc.perform(delete("/payments/{id}", paymentId)
                .contentType(MediaType.APPLICATION_JSON));

        //Then
        resultActions
                .andExpect(status().isNotFound());
    }

    private PaymentDto createPayment(String testFile) throws Exception {
        String payload = TestUtils.getResourceContent(testFile);

        ResultActions resultActions = mockMvc.perform(post("/payments")
                .content(payload)
                .contentType(MediaType.APPLICATION_JSON));

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        PaymentDto response = objectMapper.readValue(contentAsString, PaymentDto.class);

        return response;
    }

    private void deletePayment(long id) throws Exception {
        ResultActions resultActions = mockMvc.perform(delete("/payments/{id}", id)
                .contentType(MediaType.APPLICATION_JSON));
    }
}
